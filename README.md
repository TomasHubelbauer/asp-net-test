# ASP .NET Core GitLab CI Playground

## The theory

### Create a new ASP .NET Core application
https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app-xplat/index

- Download and install latest .NET Core SDK https://www.microsoft.com/net/core#windowscmd
- `mkdir funweb`
- `cd funweb`
- `dotnet new mvc`
- `code .`
- Open `HomeController.cs`
- Observer the *Output* channel in VS Code
  - Downloading package OmniSharp
  - Downloading package .NET Core Debugger
  - Installing package OmniSharp
  - Installing package .NET Core Debugger
  - Finished
- *Required assets to build and debug are missing from 'funweb'. Add them?* > *Yes*.
- *There are unresolved dependencies from 'funweb.csproj'. Please execute the restore command to continue.* > *Restore*.
- Output: *Restoring packages*, *Done*
- Press F5 to start the web application in debug mode.
- Observe the *Output* and *Debug Console* channels to check on the debugging session.
- Press Shift+F5.

### Commit the application to GitLab
- `cd funweb`
- `git init`
- Add ASP .NET Core `.gitignore` from https://stackoverflow.com/a/39289838/2715716
- Create a GitLab repository
- `git remote add origin https://gitlab.com/TomasHubelbauer/asp-net-test.git`
- `git add *`
- `git commit -m "Commit ASP .NET web application template"`
- `git pull`
- `git push -u origin master`

### Set up GitLab CI to call the `dotnet` command
https://docs.gitlab.com/ee/ci/yaml/

Merge to `deploy-test` branch to trigger the deployment.

- `git checkout -b deploy-test` to create the branch or `git checkout deploy-test` if exists
- `git pull origin master` to update `master`
- `git push origin deploy-test` to trigger deployment

Same for `stag` and `prod`.

Pushes to `master` trigget deployment to DEV - always up to date environment.

### Set up ASP .NET Core free hosting
http://www.myasp.net/ is free

https://weblog.west-wind.com/posts/2016/Jun/06/Publishing-and-Running-ASPNET-Core-Applications-with-IIS

### Upload the package to the host over FTP (can't use WebDeploy on Unix Docker image)
https://docs.gitlab.com/ee/ci/variables/#secret-variables
